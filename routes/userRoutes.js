const express = require("express");
const router = express.Router();
const auth = require("../auth");
const User = require("../models/User");
const UserController = require("../controllers/userControllers");



//Register a New User
//http://localhost:5000/api/users/register
router.post("/register", (req,res) =>{

	//Double checks if email is already registered
	User.find({email:(req.body).email}).then(result => {
		if(result.length == 0){
			UserController.registerUser(req.body).then(result => res.send(result));
		} else {
			res.send({auth: "Email already exists."})
		}
	})
});


//User Authentication (login)
router.post("/login", (req,res) => {
	UserController.loginUser(req.body).then (result => res.send(result));
});


//Retrieve all Users
router.get("/all", (req, res) =>{
	UserController.getAllUsers().then(result => res.send(result));
});

//Retrieve one User by Id
router.get("/:userId", (req, res) => {
	console.log(req.params.userId);
	UserController.getUser(req.params.userId).then(result => res.send(result))
});


module.exports = router;