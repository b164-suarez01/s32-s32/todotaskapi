const express = require("express");
const router = express.Router();
const TaskController = require("../controllers/taskControllers");
const auth = require("../auth");


//Retrieve all tasks
//http://localhost:5000/allTasks
router.get("/", (req,res) => {
	TaskController.getAllTasks().then(result => res.send(result))
});


//Create a new task
router.post("/addTask", auth.verify, (req, res) => {
	const data = {
		course: req.body}
	TaskController.addTask(data.course).then(result => res.send(result))}
	);

//Updating a Task via taskID- ADMIN only
router.patch("/:taskId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		TaskController.updateTask(req.params.taskId, req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
})


module.exports = router;