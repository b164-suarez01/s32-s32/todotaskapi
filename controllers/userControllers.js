const User = require("../models/User");
const bcrypt = require('bcrypt');
const auth = require("../auth");

//Register a User
module.exports.registerUser = (reqBody) => {

	//Creates a new User object
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		//ung 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		password: bcrypt.hashSync(reqBody.password,10)
		
	})
	//Saves the created object to our darabase
	return newUser.save().then((user,error) => {
		//if user registration failed
		if(error){
			return false;
		} else {
			//user registration is success
			return true;
		}
	})
}


//LoginUser
module.exports.loginUser = (reqBody) => {
	//findOne - it will return the first record in the collection that matches the search criteria. Kasi pag find ginamit, mas flexible siya at irereturn niya lahat ng magmamatch sa kanya

	return User.findOne({email:reqBody.email}).then(result =>{
		//User does not exist
		if (result == null){
			return false;
		}else {
			//User exists

			//the compareSync method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false"
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			//if password match
			if(isPasswordCorrect){
				//Generate n access token
				return{ accessToken : auth.createAccessToken(result.toObject())}
			} else {
				//Password does not match
				return false;
			}
		};
	});
};


//Retrieve All Users - without returning Password
module.exports.getAllUsers = () => {
	return User.find({},{password:0}).then(result => {
		result.password = "";
		return result;
	})
}

//Retrieve Specific User - without returning Password
module.exports.getUser = (reqParams) => {
	return User.findById(reqParams, {password:0}).then( result => {
		return result;
	})
}