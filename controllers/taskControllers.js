const Task = require("../models/Task");

//Retrieve all Tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result =>{
		return result;
	})
}
//Create a new Task
module.exports.addTask = (reqBody) => {
console.log(`You added ${reqBody} with an object type of ${typeof reqBody}`);

	let newTask = new Task ({
		taskName: reqBody.taskName,
		description: reqBody.description,
		dueDate: reqBody.dueDate
	});

	//saves the created task
	return newTask.save().then((task,error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}

//Update a Task
module.exports.updateTask = (taskId,reqBody) => {
	let updatedTask = {
		status: reqBody.status
	};

	//Find by Id and Update(id, updatesToBeApplied)
	return Task.findByIdAndUpdate(taskId,updatedTask).then((task,error) => {
		//task not updated
		if(error){
			return false;
		}else{
			//task updated successfully
			return true;
		}
	})
}

