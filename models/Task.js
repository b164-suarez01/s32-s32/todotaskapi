const mongoose = require("mongoose");
const taskSchema = new mongoose.Schema({

	taskName: {
		type: String,
		required: [true, "Task name is required"]
	},
	description: {
		type: String,
		require: [true,"Description is required"]
	},
	createdOn : {
		type: Date,
		default: new Date()	
	},
	dueDate : {
		type: Date,
		require: [true,"Due Date is required"]
	},
	status: {
		type: String,
		default: "Pending"
	},
	taskUser: [{
			userId:{
				type:String,
				required:[true,"UserId is required"]
			}}]
})

module.exports = mongoose.model("Task",taskSchema)